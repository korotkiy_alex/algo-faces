/**
 * Author: Oleksandr Nikitin
 * E-mail: o.nikitin@web7.com.ua
 * Date: 06.08.2017
 */
(function(){
	var classNames = {
		formGroup: 'form-group',
		hasError: 'has-error',
		hasWarning: 'has-warning',
		modalSm: 'modal-sm',

		vmModalError: 'vm-modal-error',
		vmBtnSubmit: 'vm-btn-submit'
	};
	var ids = {
        vmLoginForm: 'vmLoginForm',
	};

	var selectors = vm.buildSelectors(classNames, ids);

	var isReload = false;

	vm.clearFormElements = function() {
		var $elements = $('[name]').not('[type="hidden"], meta, [type="number"], [data-not-clear]');
		$elements.val('');
	};

	$(document).on('keypress', 'form', function(e){
		var $this = $(this);
		var isNotPressEnter = $this.attr('data-is-not-press-enter');

		if ($this.attr('method') === 'post' && e.which === 13 && !isNotPressEnter) {
			$(selectors.vmBtnSubmit).trigger('click');
			return false;
		}
	});

	$(document).on('input propertychange', 'input', function(){

	});

	$(document).on('click', selectors.vmBtnSubmit, function(e){
		var $this = $(this);
		var $thisForm = $this.attr('data-form') ? $('#' + $this.attr('data-form')) : $this.closest('form');
		var $formElementsRequired = $thisForm.find('input[required], textarea[required]');
		var $formElements = $thisForm.find('input, textarea');
		var url = $thisForm.attr('action').replace('{apiUrl}', vm.apiUrl);
		var data = {};
		isReload = $thisForm.attr('data-is-reload') && $thisForm.attr('data-is-reload') === 'true';

		$this.trigger('submit.click');

		$(selectors.formGroup).removeClass(classNames.hasError);
		$(selectors.formGroup).removeClass(classNames.hasWarning);
		$formElementsRequired.each(function(){
			var $_this = $(this);
			if (!$_this.val()) {
				$_this.closest(selectors.formGroup).addClass(classNames.hasError);
			}

			if ($_this.attr('type') === 'checkbox' && !$_this.is(':checked')) {
				$_this.closest(selectors.formGroup).addClass(classNames.hasError);
			}
		});
		if ($thisForm.find(selectors.formGroup).hasClass(classNames.hasError)) {
			var $hasError = $thisForm.find(selectors.hasError).first().find('input').focus();
			var $input = $hasError.find('input'),
				$textarea = $hasError.find('textarea');
			if ($input.exists()) {
				$input.focus();
			} else if ($textarea.exists()) {
				$textarea.focus();
			}
			return;
		}

		var dataSerialize = $thisForm.serializeArray();

		for (var dataSerializeIndex in dataSerialize) {
			var serializeItem = dataSerialize[dataSerializeIndex];
			data[serializeItem.name] = serializeItem.value || null;
		}

		// if ( $thisForm.attr('id') === ids.vmLoginForm ) {
		// 	localStorage.setItem('userLoginData', JSON.stringify(data));
		// }

        var defaultData = {
            userHash: vm.userData.userHash || '',
        };
        var params = $.extend(true, {}, defaultData, data);

		vm.ajax({
			path: url || '',
			method: $thisForm.attr('method'),
			data: params,
			triggerName: $thisForm.attr('data-trigger-name')
		}, function(_data){
			var dataCallback = $thisForm.attr('data-callback') ? vm[$thisForm.attr('data-callback')] : undefined;
			if (dataCallback) {
				if (_.isFunction(dataCallback)) {
					dataCallback(_data);
				}
			}
			if (isReload) {
				vm.isDocumentClick = true;
				location.reload();
			}
		});
		e.preventDefault();
		e.stopPropagation();
		return false;
	});

	vm.ajax = function(params, callback, modal){
		var path = params.path,
			data = params.data,
			dataType = params.dataType,
			method = params.method,
			contentType = params.contentType,
			processData = params.processData,
			triggerName = params.triggerName,
			cache = params.cache;

		if (processData === undefined) processData = true;
        if (contentType === undefined) contentType = 'application/x-www-form-urlencoded';
        if (cache === undefined) cache = true;

		vm.showThrobber();
		vm.isAjax = true;
		$.ajax({
			data: data,
			type: method || 'POST',
			url: path,
			dataType: dataType || 'json',
			contentType: contentType,
			processData: processData,
			cache: cache,
			success: function(data){
				if (data.redirect) {
					vm.isDocumentClick = true;
					location.href = data.redirect || '/';
					return;
				}
				if (_.isFunction(callback)) {
					callback(data);
				}
				if (!modal) {
                    vm.modalRemove();
				}
				if (!isReload) {
					vm.removeThrobber();
				}
				if (!_.isEmpty(triggerName)) {
					$(document).trigger(triggerName);
				}
			},
			error: function(error) {
				console.log(error);
				var dataError = error && error.responseJSON;
				var _error = dataError && dataError.error;
				var title = 'Error', errorText = 'Unknown error';
				if (_.isObject(_error)) {
					title = _error.title;
					errorText = _error.message;
				} else {
					errorText = _error || 'Unknown error';
				}
				vm.showModalDialog({
					disabledScrollbar: true,
					classSizeModal: _.isObject(_error) ? '' : classNames.modalSm,
					classModalType: classNames.vmModalError,
					modalTitle: title,
					modalBody: errorText,
					modalFooter: $('<button/>', {type: 'button', class: 'btn btn-danger', 'data-dismiss': 'modal', text: 'OK'})
				});
				vm.removeThrobber();
			}
			}).always(function() {
			vm.isAjax = false;
		});
	};
})();