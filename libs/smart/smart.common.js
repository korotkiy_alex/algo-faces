(function () {
	$.fn.exists = function () {
		return this.length !== 0;
	};

	$.fn.isFind = function($_findElement) {
		var $this = $(this);
		var $findElement = $this.find($_findElement);
		return $findElement.exists() ? $findElement : false;
	};

	window.vm = {};

	var buildSelectors = function (selectors, source, characterToPrependWith) {
		$.each(source, function (propertyName, value) {
			selectors[propertyName] = characterToPrependWith + value;
		});
	};

	vm.buildSelectors = function (classNames, ids) {
		var selectors = {};
		if (classNames) {
			buildSelectors(selectors, classNames, ".");
		}
		if (ids) {
			buildSelectors(selectors, ids, "#");
		}
		return selectors;
	};

	vm.strrev = function(string){
		var ret = '', i = 0;
		for ( i = string.length-1; i >= 0; i-- ){
			ret += string.charAt(i);
		}
		return ret;
	};

	vm.number_format = function(number, decimals, decPoint, thousandsSep) {
		decimals = Math.abs(decimals) || 0;
		number = parseFloat(number);

		if (!decPoint || !thousandsSep) {
			decPoint = '.';
			thousandsSep = ',';
		}

		var roundedNumber = Math.round(Math.abs(number) * ('1e' + decimals)) + '';
		var numbersString = decimals ? (roundedNumber.slice(0, decimals * -1) || 0) : roundedNumber;
		var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
		var formattedNumber = "";

		while (numbersString.length > 3) {
			formattedNumber += thousandsSep + numbersString.slice(-3);
			numbersString = numbersString.slice(0, -3);
		}

		if (decimals && decimalsString.length === 1) {
			while (decimalsString.length < decimals) {
				decimalsString = decimalsString + decimalsString;
			}
		}

		return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
	};

	vm.array_chunk = function(input, size) {
		for(var x, i = 0, c = -1, l = input.length, n = []; i < l; i++){
			(x = i % size) ? n[c][x] = input[i] : n[++c] = [input[i]];
		}
		return n;
	};

	vm.in_array = function(needle, haystack, strict) {
		var found = false, key, strict = !!strict;

		for (key in haystack) {
			if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
				found = true;
				break;
			}
		}

		return found;
	};

	function is_object(mixed_var) {
		if (mixed_var instanceof Array) {
			return false;
		} else {
			return (mixed_var !== null) && (typeof( mixed_var ) === 'object');
		}
	}

	vm.isObject = function (obj) {
		if (obj instanceof Array) {
			return false;
		} else {
			return (obj !== null) && (typeof( obj ) === 'object');
		}
	};

	vm.getCookie = function (name) {
		var matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		));
		return matches ? decodeURIComponent(matches[1]) : undefined;
	};
})();