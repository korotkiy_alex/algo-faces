<?php
/**
 * Created by PhpStorm.
 * User: Shnur
 * Date: 31.07.2018
 * Time: 14:14
 */

$mysqli = new mysqli('localhost', 'root', '', 'videoMetrika');

if ($mysqli->connect_errno) {
	echo "Извините, возникла проблема на сайте";
	echo "Ошибка: Не удалсь создать соединение с базой MySQL и вот почему: \n";
	echo "Номер_ошибки: " . $mysqli->connect_errno . "\n";
	echo "Ошибка: " . $mysqli->connect_error . "\n";
	exit;
}

$sql = "SELECT w.id, w.name, d.name AS department, w.foto FROM worker w LEFT JOIN department d ON w.departament_id = d.id";
if (!$result = $mysqli->query($sql)) {
	echo "Извините, возникла проблема в работе сайта.";
	echo "Ошибка: Наш запрос не удался и вот почему: \n";
	echo "Запрос: " . $sql . "\n";
	echo "Номер_ошибки: " . $mysqli->errno . "\n";
	echo "Ошибка: " . $mysqli->error . "\n";
	exit;
}

$json = array();

while ($row = $result->fetch_assoc()) {
	$json[] = array(
		'id' => $row['id'],
		'fullName' => $row['name'],
		'specialty' => $row['department'],
		'avatar' => $row['foto']
	);
}

header('Content-Type: application/json');
$resultJson = json_encode($json);
file_put_contents('../data/users.json',$resultJson);
echo $resultJson;