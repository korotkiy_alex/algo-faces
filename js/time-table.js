(function(){
    var classNames = {
        vmTimeTableHeaderTemplate: 'vm-time-table-header-template',
        vmTimeTableCellTemplate: 'vm-time-table-cell-template',
        vmTimeLineContainerTemplate: 'vm-time-line-container-template',
        vmTimeLineContainer: 'vm-time-line-container',
        vmTimeLine: 'vm-time-line',
        vmUserTimeLine: 'vm-user-time-line',
        vmIconNextDate: 'vm-icon-next-date',
        vmIconPrevDate: 'vm-icon-prev-date',

        dFlex: 'd-flex',
        alignItemsCenter: 'align-items-center',
        mr3: 'mr-3',
        overflowHidden: 'overflow-hidden',

        vmCalendarTimeTable: 'vm-calendar-table',

        col: 'col',
        vmTimeTableCell: 'vm-time-table-cell',

        positionRelative: 'position-relative',
        positionAbsolute: 'position-absolute',

        row: 'row',
        vmTimeTableHeader: 'vm-time-table-header',
        flexNowrap: 'flex-nowrap',

        vmUserItemCell: 'vm-user-item-cell',
        vmUserLogo: 'vm-user-logo',
        vmUserFullName: 'vm-user-full-name',
        vmUserSpecialty: 'vm-user-specialty',


        vmTimeTableLeftPanel: 'vm-time-table--left-panel',
        vmTimeTableRightPanel: 'vm-time-table--right-panel',

        vmScrollbar: 'vm-scrollbar',

        vmScrollTableContainer: 'vm-scroll-table-container',

        vmUserWorkTimeInfoContainer: 'vm-user-work-time-info-container',
        vmShowCalendarButton: 'vm-show-calendar-button',
        vmInTime: 'vm-in-time',
        vmOutTime: 'vm-out-time',
        vmTotalDays: 'vm-total-days',

        vmIcon: 'vm-icon',
        vmIconCalendar: 'vm-icon-calendar',

        btn: 'btn',
        btnPrimary: 'btn-primary',
        disabled: 'disabled',

        userInfo: 'user-info',
        vmVideoItemBody: 'vm-video-item-body',
        vmVideoItem: 'vm-video-item',
        vmVideoItemImage: 'vm-video-item-image',
        videoLink: 'video-link',
        materialIcons: 'material-icons',
        vmModalVideo: 'vm-modal-video',

        vmBtnSearch: 'vm-btn-search',
        vmSearchControl: 'vm-search-control',
        vmSearchNoResult: 'vm-search-no-result'
    };
    var ids = {
        vmUserWorkTimeInfoTemplate: 'vmUserWorkTimeInfoTemplate',
        vmTimeLineTemplate: 'vmTimeLineTemplate',
        departmentSearchInput: 'departmentSearchInput',
    };

    var selectors = vm.buildSelectors(classNames, ids);

    var loadData = function(searchData){
        vm.loadUsers(searchData, ttc.loadTimeTableData);
    };

    var $vmTimeTableRightPanel;
    var $vmUserItemCell;
    var $vmUserWorkTimeInfoContainer;
    var $vmTimeTableHeader;
    var $vmTimeLineTemplate;
    var $vmTimeLineContainerTemplate;

    var startDateSearch;
    var endDateSearch;

    var isNotRenderHeaderTime = false;

    var ttc = vm.timeTableController = {
        timeTableCellWidth: 0,
        timeTableData: {},
        $vmTimeTableCellTemplate: $('<div/>', {'class': [classNames.col, classNames.vmTimeTableCell].join(' ')}),
        $vmTimeTableHeaderTemplate: $('<div/>', {'class': [classNames.row, classNames.flexNowrap, classNames.positionRelative].join(' ')}),
        stepHourSize:0
    };

    ttc.loadTimeTableData = function(dataAjax) {
        var defaultData = {
            userHash: vm.userData.userHash || '',
            action: 'getTimeTable'
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);
        isNotRenderHeaderTime = false;
        $(selectors.vmTimeTableLeftPanel).html('');
        $(selectors.vmTimeTableRightPanel).html('');
        $(selectors.vmSearchNoResult).css('display', 'none');
        $(selectors.vmIconNextDate).addClass(classNames.disabled);
        $(selectors.vmIconPrevDate).addClass(classNames.disabled);
        $(selectors.vmShowCalendarButton).addClass(classNames.disabled);
        vm.showThrobber();
        vm.ajax({
            path: vm.apiUrl + '/api/time-table',
            method: 'GET',
            data: _data
        }, function(data){
            var timeTableData = {};
            var companyWorkTime = {};
            var timeStart, timeEnd;
            var showDate;
            var $vmCalendarTimeTable = $(selectors.vmCalendarTimeTable);

            if (_.isEmpty(data)) {
                return;
            }

            ttc.timeTableData = data;

            ttc.timeTableData.timeTableDataByUserId = {};

            if (ttc.timeTableData.nextDate) {
                $(selectors.vmIconNextDate).removeClass(classNames.disabled);
            }

            if (ttc.timeTableData.showDate) {
                $(selectors.vmShowCalendarButton).removeClass(classNames.disabled);
            }

            if (ttc.timeTableData.prevDate) {
                $(selectors.vmIconPrevDate).removeClass(classNames.disabled);
            }

            for (var userTimeIndex in ttc.timeTableData.userTimeLine) {
                var userTimeLine = ttc.timeTableData.userTimeLine[userTimeIndex];
                ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId] = userTimeLine;
            }


            timeTableData = vm.deepClone(ttc.timeTableData);
            if (_.isEmpty(timeTableData.companyWorkTime)) {
                timeTableData.companyWorkTime = {
                    timeStart: "00:00",
                    timeEnd: "23:00"
                }
            }

            companyWorkTime = timeTableData.companyWorkTime;

            timeStart = vm.stringHoursToTimestamp(companyWorkTime.timeStart);
            timeEnd = vm.stringHoursToTimestamp(companyWorkTime.timeEnd);

            ttc.timeStart = parseInt(companyWorkTime.timeStart.split(':')[0]);
            ttc.timeEnd = parseInt(companyWorkTime.timeEnd.split(':')[0]);
            ttc.timeLength = Math.floor((Math.abs(timeEnd - timeStart) % 86400000) / 3600000);

            // showDate = moment(data.showDate).format('DD MMMM, ddd');

            if ( typeof(data.showDate) === 'number') {
                renderDateRangePicker(data.showDate);
            } else {
                renderDateRangePicker(data.showDate.startDate, data.showDate.endDate);
            }


            // $vmCalendarTimeTable.text(showDate);

            ttc.renderLeftPanel();

            setTimeSizingParams();

            var $dataUserId = $(selectors.vmUserTimeLine + '[data-user-id]');

            $dataUserId.each(function(){
                var $this = $(this);
                ttc.renderTimeLineByUserId($this.attr('data-user-id'));
            });

            if ($dataUserId.length === 0) {
                $(selectors.vmSearchNoResult).css('display', 'block');
            }

            vm.loadUsersData('addUser', null, writeDepartmentSelect);

            $vmUserWorkTimeInfoContainer = $(selectors.vmUserWorkTimeInfoContainer);
            $vmTimeTableRightPanel = $(selectors.vmTimeTableRightPanel);
            $vmUserItemCell = $(selectors.vmUserItemCell);
            $vmTimeTableHeader = $(selectors.vmTimeTableHeader);
        });
    };

    ttc.renderTable = function(userId, $vmTimeTableCellTemplate){
        var $vmTimeTableRightPanel = $(selectors.vmTimeTableRightPanel);
        var $vmTimeTableHeader = ttc.$vmTimeTableHeaderTemplate.clone();
        var userTimeLine = ttc.timeTableData.timeTableDataByUserId[userId] || {};
        var $vmUserWorkTimeInfoContainer = ttc.$vmUserWorkTimeInfoContainerTemplate.clone();

        if (!userId && isNotRenderHeaderTime && userTimeLine === {}) {
            return;
        }

        $vmTimeTableCellTemplate.addClass(classNames.vmUserItemCell);
        $vmTimeTableCellTemplate.addClass(classNames.userInfo);
        $vmTimeTableCellTemplate.attr('data-user-id', userId);
        $vmTimeTableHeader.append($vmTimeTableCellTemplate);
        $vmTimeTableHeader.append(ttc.$vmTimeTableCellTemplate.clone());
        $vmTimeTableHeader.addClass(classNames.vmUserTimeLine);

        for (var i = ttc.timeStart; i <= ttc.timeEnd; i++) {
            var $_vmTimeTableCellTemplate = ttc.$vmTimeTableCellTemplate.clone();

            if (!isNotRenderHeaderTime) {
                var headerTime = (i > 9 ? i : '0' + i) + ':00';
                $_vmTimeTableCellTemplate.text(headerTime);
            }

            $vmTimeTableHeader.append($_vmTimeTableCellTemplate);
        }

        $vmTimeTableHeader.append(ttc.$vmTimeTableCellTemplate.clone());
        if (isNotRenderHeaderTime && userTimeLine.timeLines) {
            var timeLineStart = userTimeLine.timeLines[0];
            var timeStart = moment(timeLineStart.timeStart).format('HH[h] mm[m]');

            var timeLineEnd = userTimeLine.timeLines[userTimeLine.timeLines.length - 1];
            var timeEnd = moment(timeLineEnd.timeEnd).format('HH[h] mm[m]');

            // if (timeStart === '02h 00m') timeStart = '00h 00m';
            // if (timeEnd === '02h 00m') timeEnd = '00h 00m';

            $vmUserWorkTimeInfoContainer.find(selectors.vmInTime).text(timeStart);
            $vmUserWorkTimeInfoContainer.find(selectors.vmOutTime).text(timeEnd);
            $vmUserWorkTimeInfoContainer.find(selectors.vmTotalDays).text("1 day");
            $vmTimeTableHeader.append($vmUserWorkTimeInfoContainer).attr({'data-user-id': userId});
        }

        if (!isNotRenderHeaderTime) {
            $vmTimeTableHeader.addClass(classNames.vmTimeTableHeader).removeClass(classNames.positionRelative);
        }
        $vmTimeTableRightPanel.append($vmTimeTableHeader);

        isNotRenderHeaderTime = true;
    };

    ttc.renderTimeLineByUserId = function(userId) {
        var timeTableDataByUserId = ttc.timeTableData.timeTableDataByUserId[userId] || {};
        var timeLines = timeTableDataByUserId && timeTableDataByUserId.timeLines || [];

        if (_.isEmpty(timeLines)) {
            return;
        }

        for (var index in timeLines) {
            var timeLine = timeLines[index];
            var timeStart = moment(timeLine.timeStart).unix();
            var timeEnd = moment(timeLine.timeEnd).unix();
            var diff = timeEnd - timeStart;
            var timeStartHour = moment(timeLine.timeStart).format('HH');
            var timeStartMinutes = moment(timeLine.timeStart).format('mm');
            var timeStartHourInMinutes = Math.floor(timeStartHour * 60);
            var timeStartInMinutes = parseInt(timeStartHourInMinutes) + parseInt(timeStartMinutes) + 60;
            var $dataUserId = $(selectors.vmUserTimeLine + '[data-user-id="' + userId + '"]');

            var workLineInMinutes = Math.floor(diff / 60);

            var $vmTimeLine = $vmTimeLineContainerTemplate.clone();

            if (workLineInMinutes === 0) {
                continue;
            }

            var $itemImageEntry = $('<div/>', {'class': [classNames.vmVideoItemImage, classNames.positionRelative].join(' ')}).append(
                $('<img/>', {'src': timeLine.imgStart})
            ).append(
                $('<a/>', {
                    'data-src-video': timeLine.videoStart,
                    'class': [classNames.videoLink, classNames.positionAbsolute].join(' ')
                }).append(
                    $('<i/>', {'class': [classNames.materialIcons, classNames.positionAbsolute].join(' ')}).append('play_circle_outline')
                )
            );
            var $itemImageExit = $('<div/>', {'class': [classNames.vmVideoItemImage, classNames.positionRelative].join(' ')}).append(
                $('<img/>', {'src': timeLine.imgEnd})
            ).append(
                $('<a/>', {
                    'data-src-video': timeLine.videoEnd,
                    'class': [classNames.videoLink, classNames.positionAbsolute].join(' ')
                }).append(
                    $('<i/>', {'class': [classNames.materialIcons, classNames.positionAbsolute].join(' ')}).append('play_circle_outline')
                )
            );

            var $entryVideo = $('<div/>', {'class':classNames.vmVideoItem}).append(
                $('<p/>', {text:'Entry'})
            ).append($itemImageEntry);

            var $exitVideo = $('<div/>', {'class':classNames.vmVideoItem}).append(
                $('<p/>', {text:'Exit'})
            ).append($itemImageExit);

            var offsetMinutes = timeStartInMinutes - (ttc.timeStart * 60);

            $dataUserId.append($vmTimeLine);
            $dataUserId.find(selectors.vmTimeLineContainer).last().css({
                width: (workLineInMinutes * ttc.minuteSize) + 'px',
                // left: (timeStartInMinutes * ttc.minuteSize) + 'px'
                left: (offsetMinutes * ttc.minuteSize) + 'px'
            }).popover({
                placement: "top",
                html: true,
                trigger: 'click',
                content: $('<div/>').append($entryVideo).append($exitVideo).html()
            });
        }
    };

    ttc.renderLeftPanel = function() {
        var users = vm.deepClone(vm.usersController.users);
        var $vmTimeTableLeftPanel = $(selectors.vmTimeTableLeftPanel);
        var $_vmTimeTableHeader = ttc.$vmTimeTableHeaderFullTemplate.clone();

        $_vmTimeTableHeader.addClass(classNames.vmTimeTableHeader);
        $vmTimeTableLeftPanel.append($_vmTimeTableHeader);
        ttc.renderTable(0, ttc.$vmTimeTableCellTemplate.clone());

        for (var indexUser in users) {
            var user = users[indexUser];
            var $_vmTimeTableCellTemplate = ttc.$vmTimeTableCellTemplate.clone();
            $_vmTimeTableCellTemplate.append(
                $('<div/>', {}).append(
                    $('<figure/>', {'class': [classNames.vmUserLogo, classNames.mr3].join(' ')}).append(
                        $('<img/>', {src: user.avatar})
                    )
                ).append(
                    $('<div/>', {'class': classNames.overflowHidden}).append(
                        $('<p/>', {text: user.fullName, 'class': classNames.vmUserFullName})
                    ).append(
                        $('<p/>', {text: user.specialty, 'class': classNames.vmUserSpecialty})
                    )
                ).addClass([classNames.dFlex, classNames.alignItemsCenter].join(' '))
            );

            ttc.renderTable(user.id, $_vmTimeTableCellTemplate);
        }

        var vmScrollbar = [];
        $(selectors.vmScrollbar).each(function(){
            var ps = new PerfectScrollbar(this, {
                suppressScrollY: $(this).attr('data-scroll-y') === 'true',
                scrollYMarginOffset: parseInt($(this).attr('data-scroll-y-margin-offset')) || 0,
                scrollXMarginOffset: parseInt($(this).attr('data-scroll-x-margin-offset')) || 0
            });
            ps.update();

            vmScrollbar.push(ps);
        });
    };

    var writeDepartmentSelect = function () {
        var departments = vm.usersDataController.departmentsById;
        var $selectDepartments = $(selectors.departmentSearchInput);

        if ($selectDepartments.val() === null) {
            $selectDepartments.html("");
            $selectDepartments.append(
                $('<option/>', {value: 0, text: "-- Select Department --"})
            );
            for (indexItem in departments) {
                $selectDepartments.append(
                    $('<option/>', {value: departments[indexItem].id, text: departments[indexItem].name})
                );
            }
        }
    };

    var setTimeSizingParams = function() {
        ttc.timeTableCellWidth = $($(selectors.vmTimeTableRightPanel).find(selectors.vmTimeTableCell)[1]).outerWidth(true);
        ttc.minuteSize = ttc.timeTableCellWidth / 60;

        // console.log(ttc.timeTableCellWidth);
    };

    var renderDateRangePicker = function(showDateStart, showDateEnd){
        var $vmShowCalendarButton = $(selectors.vmShowCalendarButton);
        var $vmCalendarTimeTable = $vmShowCalendarButton.find(selectors.vmCalendarTimeTable);
        var startDate = moment(showDateStart);
        var endDate = showDateEnd ? moment(showDateEnd) : moment(showDateStart);

        var setDates = function(startDate, endDate) {
            var startTimestamp = startDate ? new Date(startDate.format('YYYY-MM-DD')).getTime() : '';
            var endTimestamp = endDate ? new Date(endDate.format('YYYY-MM-DD')).getTime() : '';

            startDateSearch = startTimestamp;
            endDateSearch = endTimestamp;

            var range = '';

            if (!startTimestamp) {
                range = ''
            }
            if (!endTimestamp) {
                range = startDate.format('DD MMMM, ddd');
            }

            if (endTimestamp && startTimestamp) {
                range = startDate.format('DD MMMM, ddd') + (endTimestamp !== startTimestamp ? ' - ' + endDate.format('DD MMMM, ddd') : '');
            }

            $vmCalendarTimeTable.html(range);
        };

        $vmShowCalendarButton.daterangepicker({
            startDate: startDate,
            endDate: endDate,
            opens: 'center',
            showSelectedRange: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')]
            },
            showCustomRangeLabel: false,
            locale: {
                applyLabel: 'Done',
                cancelLabel: 'Cancel',
                daysOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                format: 'DD MMMM, ddd'
            },
            buttonClasses: [classNames.btn].join(' '),
            applyButtonClasses: [classNames.btnPrimary].join(' ')
        }, setDates);

        setDates(startDate, endDate);

        $vmShowCalendarButton.on('apply.daterangepicker', function (e, picker) {
            if (startDateSearch === endDateSearch) {
                vm.loadUsers({
                    name:$(selectors.vmSearchControl).val(),
                    department_id: $(selectors.departmentSearchInput).val()
                }, ttc.loadTimeTableData({
                    startDate: startDateSearch,
                    endDate: endDateSearch
                }));
            } else {
                vm.loadUsers({
                    name:$(selectors.vmSearchControl).val(),
                    department_id: $(selectors.departmentSearchInput).val()
                }, ttc.loadTimeTableDataRange({
                    startDate: startDateSearch,
                    endDate: endDateSearch
                }));
            }
        })
    };

    ttc.loadTimeTableDataRange = function(dataAjax) {
        var defaultData = {
            userHash: vm.userData.userHash || '',
            action: 'getTimeTable'
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);
        isNotRenderHeaderTime = false;
        $(selectors.vmTimeTableLeftPanel).html('');
        $(selectors.vmTimeTableRightPanel).html('');
        $(selectors.vmSearchNoResult).css('display', 'none');
        $(selectors.vmIconNextDate).addClass(classNames.disabled);
        $(selectors.vmIconPrevDate).addClass(classNames.disabled);
        $(selectors.vmShowCalendarButton).addClass(classNames.disabled);
        vm.showThrobber();
        vm.ajax({
            path: vm.apiUrl + '/api/time-table',
            method: 'GET',
            data: _data
        }, function(data){
            var timeStart, timeEnd;

            if (_.isEmpty(data)) {
                return;
            }

            ttc.timeTableData = data;
            ttc.timeTableData.timeTableDataByUserId = {};

            if (ttc.timeTableData.nextDate) {
                $(selectors.vmIconNextDate).removeClass(classNames.disabled);
            }

            if (ttc.timeTableData.showDate) {
                $(selectors.vmShowCalendarButton).removeClass(classNames.disabled);
            }

            if (ttc.timeTableData.prevDate) {
                $(selectors.vmIconPrevDate).removeClass(classNames.disabled);
            }

            ttc.timeTableData.maxHours = 0;
            for (var userTimeIndex in ttc.timeTableData.userTimeLine) {
                var userTimeLine = ttc.timeTableData.userTimeLine[userTimeIndex];
                ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId] = userTimeLine;

                var userTotalTimeInMilliseconds = 0;
                var userOutTimeInMilliseconds = 0;

                var lastDate = null;
                var lastTime = null;

                for (var index in userTimeLine.timeLines) {
                    userTotalTimeInMilliseconds += userTimeLine.timeLines[index].timeEnd - userTimeLine.timeLines[index].timeStart;

                    if (lastTime && lastDate && lastDate === moment(userTimeLine.timeLines[index].timeStart).format("DD/MM/YYYY")) {
                        userOutTimeInMilliseconds += userTimeLine.timeLines[index].timeStart - lastTime;
                    }

                    lastTime = userTimeLine.timeLines[index].timeEnd;
                    lastDate = moment(userTimeLine.timeLines[index].timeStart).format("DD/MM/YYYY");
                }
                ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId].userTotalTimeInMilliseconds = userTotalTimeInMilliseconds;
                ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId].userTotalTimeInHours = Math.floor(userTotalTimeInMilliseconds / 1000 / 60 / 60);
                ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId].userOutTimeInMilliseconds = userOutTimeInMilliseconds;

                if (ttc.timeTableData.maxHours < ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId].userTotalTimeInHours) {
                    ttc.timeTableData.maxHours = ttc.timeTableData.timeTableDataByUserId[userTimeLine.userId].userTotalTimeInHours;
                }
            }

            if ( typeof(data.showDate) === 'number') {
                renderDateRangePicker(data.showDate);
            } else {
                renderDateRangePicker(data.showDate.startDate, data.showDate.endDate);
            }

            ttc.renderLeftPanelRange();

            var $dataUserId = $(selectors.vmUserTimeLine + '[data-user-id]');

            $dataUserId.each(function(){
                var $this = $(this);
                ttc.renderTimeLineRangeByUserId($this.attr('data-user-id'));
            });

            if ($dataUserId.length === 0) {
                $(selectors.vmSearchNoResult).css('display', 'block');
            }

            vm.loadUsersData('addUser', null, writeDepartmentSelect);

            $vmUserWorkTimeInfoContainer = $(selectors.vmUserWorkTimeInfoContainer);
            $vmTimeTableRightPanel = $(selectors.vmTimeTableRightPanel);
            $vmUserItemCell = $(selectors.vmUserItemCell);
            $vmTimeTableHeader = $(selectors.vmTimeTableHeader);
        });
    };

    ttc.renderLeftPanelRange = function() {
        var users = vm.deepClone(vm.usersController.users);
        var $vmTimeTableLeftPanel = $(selectors.vmTimeTableLeftPanel);
        var $_vmTimeTableHeader = ttc.$vmTimeTableHeaderFullTemplate.clone();

        $_vmTimeTableHeader.addClass(classNames.vmTimeTableHeader);
        $vmTimeTableLeftPanel.append($_vmTimeTableHeader);
        ttc.renderTableRange(0, ttc.$vmTimeTableCellTemplate.clone());

        for (var indexUser in users) {
            var user = users[indexUser];
            var $_vmTimeTableCellTemplate = ttc.$vmTimeTableCellTemplate.clone();
            $_vmTimeTableCellTemplate.append(
                $('<div/>', {}).append(
                    $('<figure/>', {'class': [classNames.vmUserLogo, classNames.mr3].join(' ')}).append(
                        $('<img/>', {src: user.avatar})
                    )
                ).append(
                    $('<div/>', {'class': classNames.overflowHidden}).append(
                        $('<p/>', {text: user.fullName, 'class': classNames.vmUserFullName})
                    ).append(
                        $('<p/>', {text: user.specialty, 'class': classNames.vmUserSpecialty})
                    )
                ).addClass([classNames.dFlex, classNames.alignItemsCenter].join(' '))
            );

            ttc.renderTableRange(user.id, $_vmTimeTableCellTemplate);
        }

        var vmScrollbar = [];
        $(selectors.vmScrollbar).each(function(){
            var ps = new PerfectScrollbar(this, {
                suppressScrollY: $(this).attr('data-scroll-y') === 'true',
                scrollYMarginOffset: parseInt($(this).attr('data-scroll-y-margin-offset')) || 0,
                scrollXMarginOffset: parseInt($(this).attr('data-scroll-x-margin-offset')) || 0
            });
            ps.update();

            vmScrollbar.push(ps);
        });
    };

    ttc.renderTableRange = function (userId, $vmTimeTableCellTemplate) {
        var $vmTimeTableRightPanel = $(selectors.vmTimeTableRightPanel);
        var $vmTimeTableHeader = ttc.$vmTimeTableHeaderTemplate.clone();
        var userTimeLine = ttc.timeTableData.timeTableDataByUserId[userId] || {};
        var $vmUserWorkTimeInfoContainer = ttc.$vmUserWorkTimeInfoContainerTemplate.clone();

        if (!userId && isNotRenderHeaderTime && userTimeLine === {}) {
            return;
        }

        $vmTimeTableCellTemplate.addClass(classNames.vmUserItemCell);
        $vmTimeTableCellTemplate.addClass(classNames.userInfo);
        $vmTimeTableCellTemplate.attr('data-user-id', userId);
        $vmTimeTableHeader.append($vmTimeTableCellTemplate);
        $vmTimeTableHeader.addClass(classNames.vmUserTimeLine);

        var koef = 100;
        var stepSize = 0;

        while (koef < ttc.timeTableData.maxHours) {
            koef += 100;
        }
        stepSize = koef / 10;
        for (var i = 0; i <= koef; i += stepSize) {
            var $_vmTimeTableCellTemplate = ttc.$vmTimeTableCellTemplate.clone();
            if (!isNotRenderHeaderTime) {
                var headerTime = i + " h";
                $_vmTimeTableCellTemplate.text(headerTime);
            }

            $vmTimeTableHeader.append($_vmTimeTableCellTemplate);
        }
        setTimeSizingParams();
        ttc.hourSize = ttc.timeTableCellWidth / stepSize;

        $vmTimeTableHeader.append(ttc.$vmTimeTableCellTemplate.clone());
        if (isNotRenderHeaderTime && !_.isEmpty(userTimeLine.timeLines)) {
            var hoursIn = ttc.timeTableData.timeTableDataByUserId[userId].userTotalTimeInMilliseconds / 1000 / 60 / 60;
            var minutesIn = Math.floor((hoursIn - Math.floor(hoursIn)) * 60);
            hoursIn = Math.floor(hoursIn);

            var hoursOut = ttc.timeTableData.timeTableDataByUserId[userId].userOutTimeInMilliseconds / 1000 / 60 / 60;
            var minutesOut = Math.floor((hoursOut - Math.floor(hoursOut)) * 60);
            hoursOut = Math.floor(hoursOut);

            var timeIn = hoursIn + "h " + (minutesIn > 9 ? minutesIn : '0' + minutesIn) + "m";
            var timeOut = hoursOut + "h " + (minutesOut > 9 ? minutesOut : '0' + minutesOut) + "m";

            $vmUserWorkTimeInfoContainer.find(selectors.vmInTime).text(timeIn);
            $vmUserWorkTimeInfoContainer.find(selectors.vmOutTime).text(timeOut);
            $vmUserWorkTimeInfoContainer.find(selectors.vmTotalDays).text(moment(startDateSearch).from(moment(endDateSearch).add(1, 'days'), true));
            $vmTimeTableHeader.append($vmUserWorkTimeInfoContainer).attr({'data-user-id': userId});
        }

        if (!isNotRenderHeaderTime) {
            $vmTimeTableHeader.addClass(classNames.vmTimeTableHeader).removeClass(classNames.positionRelative);
        }
        $vmTimeTableRightPanel.append($vmTimeTableHeader);

        isNotRenderHeaderTime = true;
    };

    ttc.renderTimeLineRangeByUserId = function (userId) {
        var timeTableDataByUserId = ttc.timeTableData.timeTableDataByUserId[userId] || {};
        var userTotalTimeInHours = timeTableDataByUserId.userTotalTimeInHours || 0;

        if (userTotalTimeInHours === 0) {
            return;
        }

        var $dataUserId = $(selectors.vmUserTimeLine + '[data-user-id="' + userId + '"]');
        var $vmTimeLine = $vmTimeLineContainerTemplate.clone();

        $dataUserId.append($vmTimeLine);
        $dataUserId.find(selectors.vmTimeLineContainer).last().css({
            width: (userTotalTimeInHours * ttc.hourSize) + 'px',
            left: '0px'
        });
    };

    var renderVideoBlock = function (src) {
        var $videoContent = $('<video/>', {
            'class': 'video-js',
            'controls': 'auto',
            'width': '100%',
            'height': '400px'
        }).append($('<source/>', {
            'src': src,
            'type': 'video/mp4'
        }));

        vm.showModalDialog({
            disabledScrollbar: true,
            classModalType: classNames.vmModalVideo,
            modalTitle: '',
            modalBody: $videoContent
        });
    };

    var onScrollTableContainer = function() {
        var position = $vmTimeTableRightPanel.position();
        $vmUserWorkTimeInfoContainer.css({right: -Math.abs(position.left) + 19});
        $vmTimeTableHeader.css({top: Math.abs(position.top - 40) - 40});
        $vmUserItemCell.css({left: Math.abs(position.left) - $vmUserItemCell.outerWidth(true)});
    };

    vm.timeTableOnLoad = function() {
        ttc.$vmUserWorkTimeInfoContainerTemplate = $($(selectors.vmUserWorkTimeInfoTemplate)[0].content.cloneNode(true));
        $vmTimeLineContainerTemplate = $($(selectors.vmTimeLineTemplate)[0].content.cloneNode(true));
        var $vmTimeTableHeaderTemplate = ttc.$vmTimeTableHeaderTemplate.clone();
        var $vmTimeTableCellTemplate = ttc.$vmTimeTableCellTemplate.clone();
        loadData({});
        ttc.$vmTimeTableHeaderFullTemplate = $vmTimeTableHeaderTemplate.append($vmTimeTableCellTemplate);
        $(selectors.vmScrollTableContainer).on('scroll', onScrollTableContainer);
    };


    // Handler for resize window
    $(document).on('resize', function () {
        setInterval(onScrollTableContainer, 100);
    });


    // Handler for search data previous day on click button previous day
    $(document).on('click', selectors.vmIconPrevDate, function(e){
        ttc.loadTimeTableData({
            startDate: ttc.timeTableData.prevDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });


    // Handler for search data next day on click button next day
    $(document).on('click', selectors.vmIconNextDate, function(e){
        ttc.loadTimeTableData({
            startDate: ttc.timeTableData.nextDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });



    $(document).on('click', selectors.vmShowCalendarButton, function(e){


        e.preventDefault();
        e.stopPropagation();
        return false;
    });


    // Handler for search workers on click search button
    $(document).on('click', selectors.vmBtnSearch, function (e) {
        if (startDateSearch === endDateSearch) {
            vm.loadUsers({
                name:$(selectors.vmSearchControl).val(),
                department_id: $(selectors.departmentSearchInput).val()
            }, ttc.loadTimeTableData({
                startDate: startDateSearch,
                endDate: endDateSearch
            }));
        } else {
            vm.loadUsers({
                name:$(selectors.vmSearchControl).val(),
                department_id: $(selectors.departmentSearchInput).val()
            }, ttc.loadTimeTableDataRange({
                startDate: startDateSearch,
                endDate: endDateSearch
            }));
        }
    });


    // Handler for render video on click video link
    $(document).on('click', selectors.videoLink, function (e) {
        renderVideoBlock($(this).data('src-video'))
    });


    // Handler for hide popover video entry and exit on click mouse
    $(document).on('mousedown', 'body', function (e) {
        var elem = e.target.closest('.popover');
        if (!elem) $(selectors.vmTimeLineContainer).popover('hide');
    });
})();