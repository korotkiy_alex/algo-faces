(function(){
    var classNames = {
        vmIconNextDate: 'vm-timelog-icon-next-date',
        vmIconPrevDate: 'vm-timelog-icon-prev-date',

        dFlex: 'd-flex',
        alignItemsCenter: 'align-items-center',
        mr3: 'mr-3',
        mr2: 'mr-2',
        overflowHidden: 'overflow-hidden',

        vmCalendarTable: 'vm-calendar-table',

        col: 'col',
        vmTimeTableCell: 'vm-time-table-cell',

        colEvent: 'col-event',
        colPhoto: 'col-photo',
        colUser: 'col-user',
        colHeaderRecords: 'col-header-records',
        colRecord: 'col-record',
        colNote: 'col-note',

        positionRelative: 'position-relative',
        positionAbsolute: 'position-absolute',

        row: 'row',
        vmTimeTableHeader: 'vm-time-table-header',
        flexNowrap: 'flex-nowrap',

        vmUserItemCell: 'vm-user-item-cell',
        vmUserLogo: 'vm-user-logo',
        vmUserFullName: 'vm-user-full-name',
        vmUserFullNameNotRecognized: 'vm-user-full-name-not-recognized',
        vmUserSpecialty: 'vm-user-specialty',

        vmIcon: 'vm-icon',
        vmIconCalendar: 'vm-icon-calendar',

        vmShowCalendarButton: 'vm-show-calendar-button',

        btn: 'btn',
        btnPrimary: 'btn-primary',
        disabled: 'disabled',

        vmScrollbar: 'vm-scrollbar',
        vmScrollTableContainer: 'vm-scroll-table-container',

        vmTimeLogTableContainer: 'vm-timelog-table-container',
        vmTimeLogTableHeader: 'vm-timelog-table-header',
        vmTimeLogTableCell: 'vm-timelog-table-cell',
        vmTimeLogItem: 'vm-timelog-item',

        textOfficeInfo: 'text-office-info',
        timeEventInfo: 'time-event-info',
        imgEvent: 'img-event',
        columnCamInfo: 'column-cam-info',
        userInfo: 'user-info',
        imageInfo: 'image-info',
        camInfo: 'cam-info',
        timelogVideoLink: 'timelog-video-link',
        vmModalVideo: 'vm-modal-video',
    };

    var ids = {};

    var selectors = vm.buildSelectors(classNames, ids);

    var $vmTimeLogTableContainer;
    var $vmTimeLogTableHeader;
    var isNotRenderHeader = false;
    var offsetTimeLogData = 0;
    var renderItemCount = 25;
    var coeffScrollLoad = 2;

    var tlc = vm.timeLogTableController = {
        timeLogData: {},
        $vmTimeLogItemTemplate: $('<tr/>', {'class': [classNames.vmTimeLogItem].join(' ')}),

        $vmTimeLogTableCellTemplate: $('<div/>', {'class': [classNames.col, classNames.vmTimeLogTableCell].join(' ')}),
        $vmTimeLogTableHeaderTemplate: $('<div/>', {'class': [classNames.row, classNames.flexNowrap, classNames.positionRelative].join(' ')})
    };

    vm.timeLogOnLoad = function(){
        loadData();
        $(selectors.vmScrollTableContainer).on('scroll', onScrollTableContainer);
    };

    var loadData = function(){
        tlc.loadTimeLogData();
    };

    tlc.loadTimeLogData = function(dataAjax) {
        var defaultData = {
            userHash: vm.userData.userHash || '',
            action: 'getTimeLog',
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);
        isNotRenderHeader = false;
        offsetTimeLogData = 0;
        $(selectors.vmTimeLogTableContainer).html('');
        $(selectors.vmIconNextDate).addClass(classNames.disabled);
        $(selectors.vmIconPrevDate).addClass(classNames.disabled);
        $(selectors.vmShowCalendarButton).addClass(classNames.disabled);
        vm.showThrobber();
        vm.ajax({
            path: vm.apiUrl + '/api/time-log',
            method: 'GET',
            data: _data
        }, function(data){
            var timeStart, timeEnd;

            if (_.isEmpty(data)) {
                return;
            }

            tlc.timeLogData = data;

            if (tlc.timeLogData.nextDate) {
                $(selectors.vmIconNextDate).removeClass(classNames.disabled);
            }

            if (tlc.timeLogData.showDate) {
                $(selectors.vmShowCalendarButton).removeClass(classNames.disabled);
            }

            if (tlc.timeLogData.prevDate) {
                $(selectors.vmIconPrevDate).removeClass(classNames.disabled);
            }

            renderDateRangePicker(data.showDate);

            tlc.renderTable(0);

            tlc.renderTimeLogData();

            var vmScrollbar = [];
            $(selectors.vmScrollbar).each(function(){
                var ps = new PerfectScrollbar(this, {
                    suppressScrollY: $(this).attr('data-scroll-y') === 'true',
                    scrollYMarginOffset: parseInt($(this).attr('data-scroll-y-margin-offset')) || 0,
                    scrollXMarginOffset: parseInt($(this).attr('data-scroll-x-margin-offset')) || 0
                });
                ps.update();

                vmScrollbar.push(ps);
            });

            $vmTimeLogTableContainer = $(selectors.vmTimeLogTableContainer);
            $vmTimeLogTableHeader = $(selectors.vmTimeLogTableHeader);
        });
    };

    tlc.renderTimeLogData = function() {

        if (offsetTimeLogData > tlc.timeLogData.eventLog.length) {
            return;
        }

        for (var i = offsetTimeLogData; i < offsetTimeLogData + renderItemCount; i++) {
            tlc.renderTable(tlc.timeLogData.eventLog[i]);
        }

        offsetTimeLogData += renderItemCount;
    };

    tlc.renderTable = function (item) {
        var $vmTimeLogTableContainer = $(selectors.vmTimeLogTableContainer);
        var $vmTimeLogTableHeader = tlc.$vmTimeLogTableHeaderTemplate.clone();
        var $vmTimeLogTableCell = tlc.$vmTimeLogTableCellTemplate.clone();

        if (!item && isNotRenderHeader) {
            return;
        }

        if (!isNotRenderHeader) {
            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
            $_vmTimeLogTableCell.text('Event');
            $_vmTimeLogTableCell.addClass(classNames.colEvent);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
            $_vmTimeLogTableCell.text('Photo');
            $_vmTimeLogTableCell.addClass(classNames.colPhoto);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
            $_vmTimeLogTableCell.text('User');
            $_vmTimeLogTableCell.addClass(classNames.colUser);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
            $_vmTimeLogTableCell.text('Records');
            $_vmTimeLogTableCell.addClass(classNames.colHeaderRecords);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
            $_vmTimeLogTableCell.text('Note');
            $_vmTimeLogTableCell.addClass(classNames.colNote);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $vmTimeLogTableHeader.addClass(classNames.vmTimeLogTableHeader).removeClass(classNames.positionRelative);
            $vmTimeLogTableContainer.append($vmTimeLogTableHeader);
        } else {

            var isRecognized = true;
            if (item.userId === 'UNRECOGNIZED') {
                item.userName = 'unrecognized';
                item.department = 'Not in database';
                isRecognized = false;
            }


            var strOfficeInfo = item.office ? item.office + ' ' + item.event : item.event;
            var $_vmTimeLogTableCell = $vmTimeLogTableCell.clone().append(
                $('<p/>', {text: strOfficeInfo, 'class': classNames.textOfficeInfo})
            ).append(
                $('<p/>', {text: moment(item.timeEvent).format('HH:mm:ss'), 'class': classNames.timeEventInfo})
            );
            $_vmTimeLogTableCell.addClass(classNames.colEvent);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);


            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone().append(
                $('<img/>', {'src': item.imgEvent, 'class': classNames.imgEvent})
            );
            $_vmTimeLogTableCell.addClass(classNames.colPhoto);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);


            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone().append(
                $('<div/>', {'class': [classNames.userInfo, classNames.dFlex, classNames.positionRelative].join(' ')}).append(
                    $('<figure/>', {'class': [classNames.vmUserLogo, classNames.mr2].join(' ')}).append(
                        $('<img/>', {src: item.avatar})
                    )
                ).append(
                    $('<div/>', {'class': classNames.overflowHidden}).append(
                        $('<p/>', {text: item.userName, 'class': isRecognized ? classNames.vmUserFullName : classNames.vmUserFullNameNotRecognized})
                    )
                )
            ).append(
                $('<p/>', {text: item.imgEventInfo, 'class': classNames.imageInfo})
            );
            $_vmTimeLogTableCell.addClass(classNames.colUser);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);


            for (var i = 1; i <= 6; i++) {
                if (item.cam == 'cam'+i) {
                    $_vmTimeLogTableCell = $vmTimeLogTableCell.clone().append(
                        $('<p/>', {text: item.cam})
                    ).append(
                        $('<a/>', {
                            'data-src-video': item.videoEvent,
                            'data-time-event': item.timeEvent,
                            'class': [classNames.timelogVideoLink, classNames.positionAbsolute].join(' '),
                            text: item.videoEventDuration
                        })
                    );
                } else {
                    $_vmTimeLogTableCell = $vmTimeLogTableCell.clone();
                }
                $_vmTimeLogTableCell.addClass(classNames.colRecord);
                $vmTimeLogTableHeader.append($_vmTimeLogTableCell);
            }


            $_vmTimeLogTableCell = $vmTimeLogTableCell.clone().append(
                $('<p/>', {text: item.comment})
            );
            $_vmTimeLogTableCell.addClass(classNames.colNote);
            $vmTimeLogTableHeader.append($_vmTimeLogTableCell);

            $vmTimeLogTableContainer.append($vmTimeLogTableHeader);
        }

        isNotRenderHeader = true;
    };

    var onScrollTableContainer = function() {
        var position = $vmTimeLogTableContainer.position();
        $vmTimeLogTableHeader.css({top: Math.abs(position.top - 40) - 40});

        if ( $(this).prop('scrollTop') * coeffScrollLoad > $(this).prop('scrollHeight') ) {
            tlc.renderTimeLogData();
            if (coeffScrollLoad > 1.2) {
                coeffScrollLoad -= 0.1;
            }
        }
    };

    var renderDateRangePicker = function (showDateStart, showDateEnd) {
        var $vmShowCalendarButton = $(selectors.vmShowCalendarButton);
        var $vmCalendarTimeTable = $vmShowCalendarButton.find(selectors.vmCalendarTable);
        var startDate = moment(showDateStart);
        var endDate = showDateEnd ? moment(showDateEnd) : moment(showDateStart);

        var setDates = function (startDate, endDate) {
            var startTimestamp = startDate ? new Date(startDate.format('YYYY-MM-DD')).getTime() : '';
            var endTimestamp = endDate ? new Date(endDate.format('YYYY-MM-DD')).getTime() : '';
            var range = '';

            if (!startTimestamp) {
                range = ''
            }
            if (!endTimestamp) {
                range = startDate.format('DD MMMM, ddd');
            }

            if (endTimestamp && startTimestamp) {
                range = startDate.format('DD MMMM, ddd') + (endTimestamp !== startTimestamp ? ' - ' + endDate.format('DD MMMM, ddd') : '');
            }

            $vmCalendarTimeTable.html(range);
        };

        $vmShowCalendarButton.daterangepicker({
            startDate: startDate,
            endDate: endDate,
            opens: 'center',
            showSelectedRange: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')]
            },
            showCustomRangeLabel: false,
            locale: {
                applyLabel: 'Done',
                cancelLabel: 'Cancel',
                daysOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                format: 'DD MMMM, ddd'
            },
            buttonClasses: [classNames.btn].join(' '),
            applyButtonClasses: [classNames.btnPrimary].join(' ')
        }, setDates);

        setDates(startDate, endDate);
    };

    var renderVideoBlock = function (src, time) {
        var $videoContent = $('<video/>', {
            'class': 'video-js',
            'controls': 'auto',
            'width': '100%',
            'height': '400px'
        }).append($('<source/>', {
            'src': src,
            'type': 'video/mp4'
        }));

        var title = moment(time).format('D MMMM YYYY HH:mm:ss');

        vm.showModalDialog({
            disabledScrollbar: true,
            classModalType: classNames.vmModalVideo,
            modalTitle: title,
            modalBody: $videoContent,
        });
    };

    $(document).on('resize', function () {
        setInterval(onScrollTableContainer, 100);
    });

    $(document).on('click', selectors.vmIconPrevDate, function(e){
        tlc.loadTimeLogData({
            startDate: tlc.timeLogData.prevDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('click', selectors.vmIconNextDate, function(e){
        tlc.loadTimeLogData({
            startDate: tlc.timeLogData.nextDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('click', selectors.timelogVideoLink, function (e) {
        renderVideoBlock($(this).data('src-video'), $(this).data('time-event'))
    });

})();