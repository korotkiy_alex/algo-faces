(function () {

    var classNames = {
        vmIconNextDate: 'vm-statistic-icon-next-date',
        vmIconPrevDate: 'vm-statistic-icon-prev-date',

        dFlex: 'd-flex',
        alignItemsCenter: 'align-items-center',
        mr2: 'mr-2',
        overflowHidden: 'overflow-hidden',
        modal: 'modal',

        vmCalendarTable: 'vm-calendar-table',

        col: 'col',

        positionRelative: 'position-relative',
        positionAbsolute: 'position-absolute',

        row: 'row',
        flexNowrap: 'flex-nowrap',

        vmShowCalendarButton: 'vm-show-calendar-button',

        vmIcon: 'vm-icon',
        vmIconCalendar: 'vm-icon-calendar',
        vmIconEditEvent: 'vm-icon-edit-event',

        materialIcons: 'material-icons',

        btn: 'btn',
        btnPrimary: 'btn-primary',
        disabled: 'disabled'
    };

    var ids = {
        totalInOffice: 'totalInOffice',
        totalOutOfOffice: 'totalOutOfOffice',
        totalLateness: 'totalLateness',
        totalOvertime: 'totalOvertime',
        averageInOffice: 'averageInOffice',
        averageOutOfOffice: 'averageOutOfOffice',
        arrivalTime: 'arrivalTime',
        leavingTime: 'leavingTime',
        vmStatisticChart: 'vmStatisticChart'
    };

    var selectors = vm.buildSelectors(classNames, ids);

    var stc = vm.statisticTableCobtroller = {
        statisticTableData: {},
    };
    
    var totalInOffice = 0;
    var totalOutOfOffice = 0;
    var totalLateness = 0;
    var totalOvertime = 0;
    var averageInOffice = 0;
    var averageOutOfOffice= 0;
    var averageArriveTime = 0;
    var averageLeaveTime = 0;

    var loadData = function () {
        stc.loadStatisticTableData();
    };

    stc.loadStatisticTableData = function (dataAjax) {
        var defaultData = {
            userHash: vm.userData.userHash || ''
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);

        totalInOffice = 0;
        totalOutOfOffice = 0;
        totalLateness = 0;
        totalOvertime = 0;
        averageInOffice = 0;
        averageOutOfOffice= 0;
        averageArriveTime = 0;
        averageLeaveTime = 0;

        $(selectors.vmArchiveTableContainer).html('');
        $(selectors.vmIconNextDate).addClass(classNames.disabled);
        $(selectors.vmIconPrevDate).addClass(classNames.disabled);
        $(selectors.vmShowCalendarButton).addClass(classNames.disabled);
        vm.showThrobber();

        vm.ajax({
            path: vm.apiUrl + '/api/statistic',
            method: 'GET',
            data: _data
        }, function (data) {
            var timeStart, timeEnd;

            if (_.isEmpty(data)) {
                return;
            }

            stc.statisticTableData = data;

            if (stc.statisticTableData.nextDate) {
                $(selectors.vmIconNextDate).removeClass(classNames.disabled);
            }

            if (stc.statisticTableData.showDate) {
                $(selectors.vmShowCalendarButton).removeClass(classNames.disabled);
            }

            if (stc.statisticTableData.prevDate) {
                $(selectors.vmIconPrevDate).removeClass(classNames.disabled);
            }

            renderDateRangePicker(data.showDate);

            stc.renderStatisticData();

            stc.renderStatisticChart();
        });
    };

    stc.renderStatisticData = function () {

        if ( typeof(stc.statisticTableData.showDate) === 'number') {
            var currentDateData = stc.statisticTableData.statistic[stc.statisticTableData.statistic.length - 1];
            totalInOffice = currentDateData.inOffice;
            totalOutOfOffice = currentDateData.OutOffice;
            totalLateness = currentDateData.lateness;
            totalOvertime = currentDateData.overTime;
            averageInOffice = currentDateData.averageInOffice;
            averageOutOfOffice = currentDateData.averageOutOfOffice;
            averageArriveTime = currentDateData.averageArriveTime;
            averageLeaveTime = currentDateData.averageLeaveTime;
        } else {
            for (indexItem in stc.statisticTableData.statistic) {
                totalInOffice += stc.statisticTableData.statistic[indexItem].inOffice;
                totalOutOfOffice += stc.statisticTableData.statistic[indexItem].OutOffice;
                totalLateness += stc.statisticTableData.statistic[indexItem].lateness;
                totalOvertime += stc.statisticTableData.statistic[indexItem].overTime;
                averageInOffice += stc.statisticTableData.statistic[indexItem].averageInOffice;
                averageOutOfOffice += stc.statisticTableData.statistic[indexItem].averageOutOfOffice;
                averageArriveTime += stc.statisticTableData.statistic[indexItem].averageArriveTime;
                averageLeaveTime += stc.statisticTableData.statistic[indexItem].averageLeaveTime;
            }

            averageArriveTime = averageArriveTime / stc.statisticTableData.statistic.length;
            averageLeaveTime = averageLeaveTime / stc.statisticTableData.statistic.length;
        }

        $(selectors.totalInOffice).html(totalInOffice);
        $(selectors.totalOutOfOffice).html(totalOutOfOffice);
        $(selectors.totalLateness).html(totalLateness);
        $(selectors.totalOvertime).html(totalOvertime);
        $(selectors.averageInOffice).html(averageInOffice);
        $(selectors.averageOutOfOffice).html(averageOutOfOffice);
        $(selectors.arrivalTime).html(moment(averageArriveTime).format('HH:mm'));
        $(selectors.leavingTime).html(moment(averageLeaveTime).format('HH:mm'));
    };

    stc.renderStatisticChart = function () {

        var data = stc.statisticTableData.statistic;

        var series = [];
        var dataInOffice = [];
        var dataOutOfOffice = [];
        var dataLateness = [];
        var dataOvertime = [];
        var dataAverageInOffice = [];
        var dataAverageOutOfOffice = [];

        for (indexItem in data) {
            dataInOffice.push(parseInt(data[indexItem].inOffice));
            dataOutOfOffice.push(parseInt(data[indexItem].OutOffice));
            dataLateness.push(parseInt(data[indexItem].lateness));
            dataOvertime.push(parseInt(data[indexItem].overTime));
            dataAverageInOffice.push(parseInt(data[indexItem].averageInOffice));
            dataAverageOutOfOffice.push(parseInt(data[indexItem].averageOutOfOffice));
        }

        series.push({name: "Total in office", data: dataInOffice});
        series.push({name: "Total out of office", data: dataOutOfOffice});
        series.push({name: "Total lateness", data: dataLateness});
        series.push({name: "Total overtime", data: dataOvertime});
        series.push({name: "Average in office", data: dataAverageInOffice});
        series.push({name: "Average out of office", data: dataAverageOutOfOffice});

        Highcharts.chart(ids.vmStatisticChart, {

            title: {
                text: ''
            },

            subtitle: {
                text: ''
            },

            chart: {
                height: 525,
                type: 'line'
            },

            yAxis: {
                title: {
                    text: 'Hours'
                }
            },

            xAxis: {
                labels: {
                    format: "{value:%d %B}"
                },
                type: "datetime"
            },

            legend: {
                layout: "horizontal",
                align: "center",
                verticalAlign: "bottom",
                floating: false,
                itemStyle: {
                    fontFamily: "\"Lucida Grande\", \"Lucida Sans Unicode\", Verdana, Arial, Helvetica, sans-serif",
                    color: "#333333",
                    fontSize: "12px",
                    fontWeight: "normal",
                    fontStyle: "normal",
                    cursor: "pointer"
                }
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: data[0].date,
                    pointInterval: 24 * 3600 * 1000
                }
            },

            series: series,

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {}
                    }
                }]
            }

        });
    };

    var renderDateRangePicker = function (showDateStart, showDateEnd) {
        var $vmShowCalendarButton = $(selectors.vmShowCalendarButton);
        var $vmCalendarTimeTable = $vmShowCalendarButton.find(selectors.vmCalendarTable);
        var startDate = moment(showDateStart);
        var endDate = showDateEnd ? moment(showDateEnd) : moment(showDateStart);

        var setDates = function (startDate, endDate) {
            var startTimestamp = startDate ? new Date(startDate.format('YYYY-MM-DD')).getTime() : '';
            var endTimestamp = endDate ? new Date(endDate.format('YYYY-MM-DD')).getTime() : '';
            var range = '';

            if (!startTimestamp) {
                range = ''
            }
            if (!endTimestamp) {
                range = startDate.format('DD MMMM, ddd');
            }

            if (endTimestamp && startTimestamp) {
                range = startDate.format('DD MMMM, ddd') + (endTimestamp !== startTimestamp ? ' - ' + endDate.format('DD MMMM, ddd') : '');
            }

            $vmCalendarTimeTable.html(range);
        };

        $vmShowCalendarButton.daterangepicker({
            startDate: startDate,
            endDate: endDate,
            opens: 'center',
            showSelectedRange: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')]
            },
            showCustomRangeLabel: false,
            locale: {
                applyLabel: 'Done',
                cancelLabel: 'Cancel',
                daysOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                format: 'DD MMMM, ddd'
            },
            buttonClasses: [classNames.btn].join(' '),
            applyButtonClasses: [classNames.btnPrimary].join(' ')
        }, setDates);

        setDates(startDate, endDate);
    };

    vm.statisticOnLoad = function () {
        loadData();
    };

    $(document).on('click', selectors.vmIconPrevDate, function (e) {
        stc.loadStatisticTableData({
            startDate: stc.statisticTableData.prevDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('click', selectors.vmIconNextDate, function (e) {
        stc.loadStatisticTableData({
            startDate: stc.statisticTableData.nextDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });
}());