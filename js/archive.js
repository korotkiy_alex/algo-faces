(function () {
    var classNames = {
        vmIconNextDate: 'vm-archive-icon-next-date',
        vmIconPrevDate: 'vm-archive-icon-prev-date',

        vmArchiveTableContainer: 'vm-archive-table-container',
        vmArchiveItem: 'vm-archive-item',
        vmArchiveItemBody: 'vm-archive-item-body',
        vmArchiveItemImage: 'archive-item-image',
        vmArchiveBlockInfo: 'archive-block-info',
        archiveVideoLink: 'archive-video-link',
        eventInfo: 'event-info',
        userInfo: 'user-info',

        vmUserLogo: 'vm-user-logo',
        vmUserFullName: 'vm-user-full-name',
        vmUserFullNameNotRecognized: 'vm-user-full-name-not-recognized',
        vmUserSpecialty: 'vm-user-specialty',
        vmOfficeInfo: 'vm-office-info',
        vmTimeEventInfo: 'vm-time-event-info',


        dFlex: 'd-flex',
        alignItemsCenter: 'align-items-center',
        mr2: 'mr-2',
        overflowHidden: 'overflow-hidden',
        modal: 'modal',
        fade: 'fade',
        videoModal: 'video-modal',

        vmCalendarTable: 'vm-calendar-table',

        col: 'col',

        positionRelative: 'position-relative',
        positionAbsolute: 'position-absolute',

        row: 'row',
        flexNowrap: 'flex-nowrap',

        vmShowCalendarButton: 'vm-show-calendar-button',

        vmIcon: 'vm-icon',
        vmIconCalendar: 'vm-icon-calendar',
        vmIconEditEvent: 'vm-icon-edit-event',

        materialIcons: 'material-icons',

        btn: 'btn',
        btnPrimary: 'btn-primary',
        disabled: 'disabled',

        modalSm: 'modal-sm',

        vmModalError: 'vm-modal-error',
        vmModalVideo: 'vm-modal-video',
        vmBtnSubmit: 'vm-btn-submit',

        vmScrollbar: 'vm-scrollbar',
        vmScrollTableContainer: 'vm-scroll-table-container',
    };

    var ids = {
        archiveVideo: 'archiveVideo-',
        vmArchiveVideoBlock: 'vmArchiveVideoBlock',
    };

    var selectors = vm.buildSelectors(classNames, ids);

    var $vmArchiveTableContainer;
    var offsetArchiveTableData = 0;
    var renderItemCount = 25;
    var coeffScrollLoad = 2;

    var atc = vm.archiveTableController = {
        archiveTableData: {},
        $vmArchiveItemTemplate: $('<div/>', {'class': [classNames.vmArchiveItem].join(' ')}).append($('<div/>', {'class': [classNames.vmArchiveItemBody].join(' ')}))
    };

    vm.archiveOnLoad = function () {
        loadData();
        $(selectors.vmScrollTableContainer).on('scroll', onScrollTableContainer);
    };

    var loadData = function () {
        atc.loadArchiveTableData();
    };

    atc.loadArchiveTableData = function (dataAjax) {
        var defaultData = {
            userHash: vm.userData.userHash || ''
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);
        offsetArchiveTableData = 0;
        $(selectors.vmArchiveTableContainer).html('');
        $(selectors.vmIconNextDate).addClass(classNames.disabled);
        $(selectors.vmIconPrevDate).addClass(classNames.disabled);
        $(selectors.vmShowCalendarButton).addClass(classNames.disabled);
        vm.showThrobber();
        vm.ajax({
            path: vm.apiUrl + '/api/archive',
            method: 'GET',
            data: _data
        }, function (data) {
            var timeStart, timeEnd;

            if (_.isEmpty(data)) {
                return;
            }

            atc.archiveTableData = data;

            if (atc.archiveTableData.nextDate) {
                $(selectors.vmIconNextDate).removeClass(classNames.disabled);
            }

            if (atc.archiveTableData.showDate) {
                $(selectors.vmShowCalendarButton).removeClass(classNames.disabled);
            }

            if (atc.archiveTableData.prevDate) {
                $(selectors.vmIconPrevDate).removeClass(classNames.disabled);
            }

            renderDateRangePicker(data.showDate);

            atc.renderArchiveData();

            var vmScrollbar = [];
            $(selectors.vmScrollbar).each(function(){
                var ps = new PerfectScrollbar(this, {
                    suppressScrollY: $(this).attr('data-scroll-y') === 'true',
                    scrollYMarginOffset: parseInt($(this).attr('data-scroll-y-margin-offset')) || 0,
                    scrollXMarginOffset: parseInt($(this).attr('data-scroll-x-margin-offset')) || 0
                });
                ps.update();

                vmScrollbar.push(ps);
            });
        });
    };

    atc.renderArchiveData = function () {
        if (offsetArchiveTableData > atc.archiveTableData.eventLog.length) {
            return;
        }

        for (var i = offsetArchiveTableData; i < offsetArchiveTableData + renderItemCount; i++) {
            atc.renderArchiveItem(atc.archiveTableData.eventLog[i]);
        }

        offsetArchiveTableData += renderItemCount;
    };

    atc.renderArchiveItem = function (item) {
        if (!item) {
            return;
        }

        var $vmArchiveTableContainer = $(selectors.vmArchiveTableContainer);
        var $itemTemplate = atc.$vmArchiveItemTemplate.clone();
        var $itemBody = $itemTemplate.find(selectors.vmArchiveItemBody);

        var $itemImage = $('<div/>', {'class': [classNames.vmArchiveItemImage, classNames.positionRelative].join(' ')}).append(
            $('<img/>', {'src': item.imgEvent})
        ).append(
            $('<a/>', {
                'data-src-video': item.videoEvent,
                'data-time-event': item.timeEvent,
                'class': [classNames.archiveVideoLink, classNames.positionAbsolute].join(' ')
            }).append(
                $('<i/>', {'class': [classNames.materialIcons, classNames.positionAbsolute].join(' ')}).append('play_circle_outline')
            )
        );

        var $itemInfo = $('<div/>', {'class': [classNames.vmArchiveBlockInfo].join(' ')}).append(
            $('<div/>', {'class': [classNames.eventInfo, classNames.dFlex, classNames.positionRelative].join(' ')})
        ).append(
            $('<div/>', {'class': [classNames.userInfo, classNames.dFlex, classNames.positionRelative].join(' ')})
        );

        var $blockEventInfo = $itemInfo.find(selectors.eventInfo);
        var $blockUserInfo = $itemInfo.find(selectors.userInfo);

        var strOfficeInfo = item.office ? item.office + ' ' + item.event : item.event;
        $blockEventInfo.append(
            $('<p/>', {'class': [classNames.vmOfficeInfo].join(' ')}).append(strOfficeInfo)
        ).append(
            $('<p/>', {'class': [classNames.vmTimeEventInfo, classNames.positionAbsolute].join(' ')}).append(moment(item.timeEvent).format('HH:mm:ss'))
        );

        var isRecognized = true;

        if (item.userId === 'UNRECOGNIZED') {
            item.userName = 'unrecognized';
            item.department = 'Not in database';
            isRecognized = false;
        }

        $blockUserInfo.append(
            $('<figure/>', {'class': [classNames.vmUserLogo, classNames.mr2].join(' ')}).append(
                $('<img/>', {src: item.avatar})
            )
        ).append(
            $('<div/>', {'class': classNames.overflowHidden}).append(
                $('<p/>', {text: item.userName, 'class': isRecognized ? classNames.vmUserFullName : classNames.vmUserFullNameNotRecognized})
            ).append(
                $('<p/>', {text: item.department, 'class': classNames.vmUserSpecialty})
            )
        ).append(
            $('<a/>', {
                'href': '#',
                'class': [classNames.vmIcon, classNames.vmIconEditEvent, classNames.positionAbsolute].join(' '),
                'data-event-id': item.id_event
            }).append(
                $('<i/>', {'class': [classNames.materialIcons].join(' ')}).append('edit')
            )
        );

        $itemBody.append($itemImage).append($itemInfo);

        $vmArchiveTableContainer.append($itemTemplate);
    };

    var onScrollTableContainer = function() {
        if ( $(this).prop('scrollTop') * coeffScrollLoad > $(this).prop('scrollHeight') ) {
            atc.renderArchiveData();
            if (coeffScrollLoad > 1.2) {
                coeffScrollLoad -= 0.1;
            }
        }
    };

    var renderVideoBlock = function (src, time) {
        var $videoContent = $('<video/>', {
            'class': 'video-js',
            'controls': 'auto',
            'width': '100%',
            'height': '400px'
        }).append($('<source/>', {
            'src': src,
            'type': 'video/mp4'
        }));

        var title = moment(time).format('D MMMM YYYY HH:mm:ss');

        vm.showModalDialog({
            disabledScrollbar: true,
            classModalType: classNames.vmModalVideo,
            modalTitle: title,
            modalBody: $videoContent,
        });
    };

    var renderDateRangePicker = function (showDateStart, showDateEnd) {
        var $vmShowCalendarButton = $(selectors.vmShowCalendarButton);
        var $vmCalendarTimeTable = $vmShowCalendarButton.find(selectors.vmCalendarTable);
        var startDate = moment(showDateStart);
        var endDate = showDateEnd ? moment(showDateEnd) : moment(showDateStart);

        var setDates = function (startDate, endDate) {
            var startTimestamp = startDate ? new Date(startDate.format('YYYY-MM-DD')).getTime() : '';
            var endTimestamp = endDate ? new Date(endDate.format('YYYY-MM-DD')).getTime() : '';
            var range = '';

            if (!startTimestamp) {
                range = ''
            }
            if (!endTimestamp) {
                range = startDate.format('DD MMMM, ddd');
            }

            if (endTimestamp && startTimestamp) {
                range = startDate.format('DD MMMM, ddd') + (endTimestamp !== startTimestamp ? ' - ' + endDate.format('DD MMMM, ddd') : '');
            }

            $vmCalendarTimeTable.html(range);
        };

        $vmShowCalendarButton.daterangepicker({
            startDate: startDate,
            endDate: endDate,
            opens: 'center',
            showSelectedRange: false,
            showDropdowns: true,
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')]
            },
            showCustomRangeLabel: false,
            locale: {
                applyLabel: 'Done',
                cancelLabel: 'Cancel',
                daysOfWeek: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                format: 'DD MMMM, ddd'
            },
            buttonClasses: [classNames.btn].join(' '),
            applyButtonClasses: [classNames.btnPrimary].join(' ')
        }, setDates);

        setDates(startDate, endDate);
    };

    $(document).on('click', selectors.vmIconPrevDate, function (e) {
        atc.loadArchiveTableData({
            startDate: atc.archiveTableData.prevDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('click', selectors.vmIconNextDate, function (e) {
        atc.loadArchiveTableData({
            startDate: atc.archiveTableData.nextDate
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('click', selectors.archiveVideoLink, function (e) {
        renderVideoBlock($(this).data('src-video'), $(this).data('time-event'))
    });
})();