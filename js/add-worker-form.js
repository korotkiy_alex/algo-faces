(function(){

    var classNames = {
        vmAddNewUser: 'vm-add-new-user',

        vmModalAddNewUser: 'vm-modal-add-new-user',

        vmWorkerPhoto: 'vm-worker-photo',
        vmInputWorkerPhoto: 'vm-input-worker-photo',
        deletePhoto: 'delete-photo',
        addOffice: 'add-office',

        btn: 'btn',
        btnInfo: 'btn-info',
        btnDanger: 'btn-danger',
        btnPrimary: 'btn-primary',
        selectsOpenDoor: 'selects-open-door',

        non: 'non',
        editable: 'editable',

        vmBtnSubmit: 'vm-btn-submit-save-user',
        hasError: 'vm-has-error',
        
        userInfo: 'user-info'
    };

    var ids = {
        vmAddNewUserTemplate: 'vmAddNewUserTemplate',
        phoneInput: 'phoneInput',
        departmentInput: 'departmentInput',
        btnSaveUser: 'btnSaveUser',
        saveOffice: 'saveOffice',
        vmListOffices: 'vmListOffices',
        newOfficeName: 'newOfficeName',
        modalAddOfiice: 'modalAddOfiice',
        formSaveUser: 'formSaveUser',
        errorInputNameOffice: 'errorInputNameOffice',

        vmWorkerPhoto1: 'vm-worker-photo-1',
        vmWorkerPhoto2: 'vm-worker-photo-2',
        vmWorkerPhoto3: 'vm-worker-photo-3',
        vmWorkerPhoto4: 'vm-worker-photo-4',
    };

    var selectors = vm.buildSelectors(classNames, ids);

    var departments;
    var offices;

    var renderAddUserForm = function () {
        var $template = $(selectors.vmAddNewUserTemplate).clone();

        departments = vm.usersDataController.departmentsById;
        offices = vm.usersDataController.officesById;
        userData = vm.usersDataController.userData;

        var title = userData ? 'Edit worker' : 'New worker';

        vm.showModalDialog({
            disabledScrollbar: true,
            classModalType: classNames.vmModalAddNewUser,
            modalTitle: title,
            modalBody: $template.html(),
            modalButtons: [
                {
                    btnName: 'Save',
                    btnClass: [classNames.btn, classNames.btnPrimary, classNames.vmBtnSubmit].join(" "),
                    btnAttr: [{name:'data-form', value:ids.formSaveUser}],
                }
            ],
        });

        $(selectors.phoneInput).mask("+38 (000) 000-00-00");

        var $selectDepartments = $(selectors.departmentInput);
        $selectDepartments.html("");
        for (indexItem in departments) {
            $selectDepartments.append(
                $('<option/>', {value: departments[indexItem].id, text: departments[indexItem].name})
            );
        }

        refreshSelects(userData.availableOffices);

        var initialText = $(selectors.editable).val();
        $(selectors.newOfficeName).val(initialText);

        $selectDepartments.val(userData.departament_id);

        if (userData) {
            $('input[name="id"]').val(userData.id);
            $('input[name="fio_ru"]').val(userData.fio_ru);
            $('input[name="email"]').val(userData.email);
            $('input[name="phone"]').val(userData.phone);

            $(selectors.vmWorkerPhoto1).attr("src", userData.avatar);
            $(selectors.vmWorkerPhoto2).attr("src", userData.neuron_photo1);
            $(selectors.vmWorkerPhoto3).attr("src", userData.neuron_photo2);
            $(selectors.vmWorkerPhoto4).attr("src", userData.neuron_photo3);
        }
    };

    var refreshSelects = function (availableOffices) {
        var $selectsOffice = $('select[name="office[]"]');
        var $listOffices = $(selectors.vmListOffices);
        $listOffices.html("");
        $selectsOffice.each(function (key, elem) {
            $(elem).html("");
            for (indexItem in offices) {
                $(elem).append(
                    $('<option/>', {value: offices[indexItem].id, text: offices[indexItem].office})
                );
            }
        });
        for (indexItem in offices) {
            $($listOffices).append(
                $('<option/>', {class: [classNames.non].join(" "), value: offices[indexItem].id, text:offices[indexItem].office})
            );
        }

        $($listOffices).append(
            $('<option/>', {class: [classNames.editable].join(" "), value: "Other...", text:"Other..."})
        );

        $(selectors.selectsOpenDoor).html("");

        if (availableOffices) {
            for (indexOffice in availableOffices) {
                var $select = $selectsOffice.clone();
                $select.val(availableOffices[indexOffice]);
                $(selectors.selectsOpenDoor).append($select);
            }
        }

    };

    var renderPhoto = function (input) {
        var $image = $(input).siblings(selectors.vmWorkerPhoto);
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $image.attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    };

    var addOffice = function(dataAjax) {
        for (indexOffice in offices) {
            if ( dataAjax.office == offices[indexOffice].office ) {
                $(selectors.errorInputNameOffice).remove();
                $(selectors.newOfficeName).after($('<span/>', {id:ids.errorInputNameOffice, style:'color: red;', text: 'This name office already exists!'}));
                return;
            }
        }

        $(selectors.errorInputNameOffice).remove();

        var defaultData = {
            userHash: vm.userData.userHash || '',
            action: 'addOffice'
        };
        var _data = $.extend(true, {}, defaultData, dataAjax);
        vm.ajax({
            path: vm.apiUrl + '/api/users?userHash=' + defaultData.userHash,
            method: 'POST',
            data: _data
        }, function (data) {
            if (data) {
                vm.usersDataController.offices.push({id: data, office: dataAjax.office});
                vm.usersDataController.officesById[data] = {id: data, office: dataAjax.office};
                refreshSelects();
                var $selectOffice = $($('select[name="office[]"]')[0]).clone();
                $selectOffice.val(data);
                $(selectors.selectsOpenDoor).append($selectOffice);
            }
            $(selectors.modalAddOfiice).modal("hide");
        }, true);
    };

    vm.saveUserCallback = function(data){
        if (data && data.responseCode === 'OK') {
            console.log(data);
        }
    };

    $(document).on('click', selectors.vmAddNewUser, function (e) {
        vm.loadUsersData('addUser', null, renderAddUserForm);
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('change', selectors.vmInputWorkerPhoto, function (e) {
        renderPhoto(this);
    });

    $(document).on('click', selectors.deletePhoto, function (e) {
        $(this).siblings(selectors.vmInputWorkerPhoto).val("");
        $(this).siblings(selectors.vmWorkerPhoto).attr("src", "");
        e.preventDefault();
        e.stopPropagation();
        return false;
    });

    $(document).on('change', selectors.vmListOffices, function (e) {
        var selected = $('option:selected', this).attr('class');
        var optionText = $(selectors.editable).text();

        if (selected === "editable") {
            $(selectors.newOfficeName).show();

            $(selectors.newOfficeName).keyup(function(){
                var editText = $(selectors.newOfficeName).val();
                $(selectors.editable).val(editText);
                $(selectors.editable).html(editText);
            });
        } else {
            $(selectors.newOfficeName).hide();
        }
    });

    $(document).on('click', selectors.saveOffice, function (e) {
        var office = $(selectors.vmListOffices).val();
        if (offices[office] === undefined) {
            addOffice({office: office});
        } else {
            var $selectOffice = $($('select[name="office[]"]')[0]).clone();
            $selectOffice.val(office);
            $(selectors.selectsOpenDoor).append($selectOffice);
            $(selectors.modalAddOfiice).modal("hide");
        }
    });

    $(document).on('click', selectors.vmBtnSubmit, function(e){
        var $this = $(this);
        var $thisForm = $this.attr('data-form') ? $('#' + $this.attr('data-form')) : $this.closest('form');
        var $formElements = $thisForm.find('input, textarea, select');
        var url = $thisForm.attr('action').replace('{apiUrl}', vm.apiUrl).replace('{userHash}', vm.userData.userHash);
        var data = {};
        isReload = $thisForm.attr('data-is-reload') && $thisForm.attr('data-is-reload') === 'true';

        $this.trigger('submit.click');

        data = new FormData();

        var dataSerialize = $thisForm.serializeArray();

        for (var dataSerializeIndex in dataSerialize) {
            var serializeItem = dataSerialize[dataSerializeIndex];
            data.append(serializeItem.name, serializeItem.value || null);
        }

        var offices = [];
        $formElements.each(function(key, elem) {
            if (elem.name === 'office[]') {
                offices.push($(elem).val());
            }

            if (elem.name === 'photo') data.append('photo', elem.files[0] || null);
            if (elem.name === 'neuron_photo1') data.append('neuron_photo1', elem.files[0] || null);
            if (elem.name === 'neuron_photo2') data.append('neuron_photo2', elem.files[0] || null);
            if (elem.name === 'neuron_photo3') data.append('neuron_photo3', elem.files[0] || null);
        });

        data.append('available_offices', JSON.stringify(offices));
        data.append('office_id', null);

        vm.ajax({
            path: url || '',
            method: $thisForm.attr('method'),
            data: data,
            triggerName: $thisForm.attr('data-trigger-name'),
            cache : false,
            processData: false,
            contentType: false,
        }, function(_data){
            var dataCallback = $thisForm.attr('data-callback') ? vm[$thisForm.attr('data-callback')] : undefined;
            if (dataCallback) {
                if (_.isFunction(dataCallback)) {
                    dataCallback(_data);
                }
            }
            if (isReload) {
                vm.isDocumentClick = true;
                location.reload();
            }
        });
        e.preventDefault();
        e.stopPropagation();
        return false;
    });
    
    $(document).on('click', selectors.userInfo, function (e) {
        var $this = $(this);
        vm.loadUsersData('editUser', $this.data('user-id'), renderAddUserForm);
    });

})();