(function(){
	if (!$.isFunction($.fn.exists)) {
		$.fn.exists = function () {
			return this.length !== 0;
		};
	}

	window.vm = {
		userData: {},
		usersController: {
			usersById: {},
			users: []
		},
		usersDataController: {
			departments: [],
			departmentsById: [],
			offices: [],
			officesById: [],
			userData: {
				id: 0
			}
		},
		// apiUrl: 'http://www.startup-api.com'
		apiUrl: 'http://192.168.90.4'
	};

	var classNames = {
		dropdownToggle: 'dropdown-toggle',
		btnLogin: 'btn-login'
	};

	var ids = {};

	var uc = vm.usersController;
	var udc = vm.usersDataController;

	var buildSelectors = function (selectors, source, characterToPrependWith) {
		$.each(source, function (propertyName, value) {
			selectors[propertyName] = characterToPrependWith + value;
		});
	};

	vm.buildSelectors = function (classNames, ids) {
		var selectors = {};
		if (classNames) {
			buildSelectors(selectors, classNames, ".");
		}
		if (ids) {
			buildSelectors(selectors, ids, "#");
		}
		return selectors;
	};

	var selectors = vm.buildSelectors(classNames, ids);

	vm.showThrobber = function(){
		if ($('.bs-throbber-container').exists()) {
			return;
		}
		$.showThrobber({
			faThrobberClass: 'fa-circle-o-notch',
			colorClass: 'text-dark'
		});
	};

	vm.removeThrobber = $.removeThrobber;

	vm.deepClone = function(object) {
		if(_.isArray(object)) {
			return jQuery.extend(true, [], object);
		} else {
			return jQuery.extend(true, {}, object);
		}
	};

	vm.stringHoursToTimestamp = function(_this) {
		var time = _this.split(':');
		var date = new Date(0, 0, 0, time[0], time[1]);
		return date.getTime();
	};

	vm.loadUsers = function(searchData, callback) {
		vm.showThrobber();
		$.ajax({
			url: vm.apiUrl + '/api/users',
			data: {
				action: 'getUsers',
				userHash: vm.userData.userHash || '',
				searchName: searchData.name || '',
				departament_id: searchData.department_id || ''
			},
			type: 'GET',
			dataType: 'json'
		}).done(function(data){
			var callCallback = false;

			if (_.isEmpty(data)) {
				if (!_.isEmpty(searchData)) {
                    if ($.isFunction(callback)) {
                        callCallback = true;
                        callback();
                    }
				} else {
                    return;
				}
			}

			uc.users = data;
			for (var indexUser in uc.users) {
				var user = uc.users[indexUser];
				uc.usersById[user.id] = user;
			}

			if ($.isFunction(callback) && !callCallback) {
				callback();
			}
		}).fail(function(error){});
	};

	vm.loadUsersData = function(action, user_id, callback) {
        vm.showThrobber();
        $.ajax({
            url: vm.apiUrl + '/api/users',
            data: {
                action: action,
				id: user_id || 0,
                userHash: vm.userData.userHash || ''
            },
            type: 'GET',
            dataType: 'json'
        }).done(function(data){
            if (_.isEmpty(data)) {
                return;
            }

            udc.departments = data.departments;
            udc.offices = data.offices;

            for (var indexDepartment in udc.departments) {
                var department = udc.departments[indexDepartment];
                udc.departmentsById[department.id] = department;
            }
            for (var indexOffice in udc.offices) {
                var office = udc.offices[indexOffice];
                udc.officesById[office.id] = office;
            }

            udc.userData = data.userData;
            udc.userData.id = user_id;

            if ($.isFunction(callback)) {
                callback();
            }
        }).fail(function(error){});
	};

	// vm.routerCallback = function(data, namePage) {
     //    if (data && data.responseCode === 'OK') {
     //        vm.userData = data.responseData || {};
     //        localStorage.setItem('userData', JSON.stringify(data.responseData));
     //        page(namePage);
     //    } else {
     //        page('login');
	// 	}
	// };

	vm.loginCallback = function(data){
		if (data && data.responseCode === 'OK') {
			vm.userData = data.responseData || {};
			localStorage.setItem('userData', JSON.stringify(data.responseData));
			page('time_table');
		}
	};

	$(function(){
        // var userLoginData = JSON.parse(localStorage.getItem('userLoginData')) || {};
        //
        // if ( _.isEmpty(userLoginData) && window.location.pathname !== '/login' ) {
        //     page('login');
        // }
        //
        // $.ajax({
        //     url: vm.apiUrl + '/api/session',
        //     data: userLoginData,
        //     type: 'POST',
        //     dataType: 'json'
        // }).done(function(data){
        //     if (_.isEmpty(data)) {
        //         return;
        //     }
        //     namePage = window.location.pathname.substr(1);
        //     vm.routerCallback(data, namePage);
        // }).fail(function(error){});
	});
})();