(function () {
	// private api

	var cache = {};

	function get(url, cb) {
		if (cache[url]) return cb(cache[url]);
		$.ajax({
			url: url,
			success: function (data) {
				cache[url] = data;
				cb(data);
			},
			error: function (jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown);
			},
			dataType: 'text'
		});
	}

	// public api

	window.init = {
		ctx: function (ctx, next) {
			ctx.data = {};
			ctx.partials = {};
			next();
		}
	};

	window.route = {
		login: function (ctx, next) {
			get('views/login.html', function (html) {
				ctx.data.index = 0;
				ctx.partials.content = html;
				next();
			});
		},
		time_table: function (ctx, next) {
			get('views/time_table.html', function (html) {
				var template = Hogan.compile(html),
					content = template.render(ctx.data, ctx.partials);

				$('#content').removeClass('vm-login-background').empty().append(content);
				vm.timeTableOnLoad();
			});
		},
		time_log: function (ctx, next) {
			get('views/time_log.html', function (html) {
				var template = Hogan.compile(html),
					content = template.render(ctx.data, ctx.partials);

				$('#content').removeClass('vm-login-background').empty().append(content);
				vm.timeLogOnLoad();
			});
		},
		archive: function (ctx, next) {
			get('views/archive.html', function (html) {
				var template = Hogan.compile(html),
					content = template.render(ctx.data, ctx.partials);

				$('#content').removeClass('vm-login-background').empty().append(content);
				vm.archiveOnLoad();
			});
		},
		statistic: function(ctx, next) {
			get('views/statistic.html', function (html) {
				var template = Hogan.compile(html),
					content =  template.render(ctx.data, ctx.partials);

				$('#content').removeClass('vm-login-background').empty().append(content);
				vm.statisticOnLoad();
            });
		},
		setup: function (ctx, next) {
			get('views/setup.html', function (html) {
				var template = Hogan.compile(html),
					content = template.render(ctx.data, ctx.partials);

				$('#content').removeClass('vm-login-background').empty().append(content);
				vm.setupOnLoad();
			});
		}
	};

	window.render = {
		content: function (ctx, next) {
			get('views/login.html', function (html) {
				var template = Hogan.compile(html),
					content = template.render(ctx.data, ctx.partials);

				$('#content').addClass('vm-login-background').empty().append(content);
				if (typeof done === 'function') {
					done(ctx.data.index);
				}
			});
		}
	};

	window.done = null;

	$(function(){
		page.base(location.pathname);
		page('*', init.ctx);
		page('login', route.login);
		page('time_table', route.time_table);
		page('time_log', route.time_log);
		page('archive', route.archive);
		page('statistic', route.statistic);
		page('setup', route.setup);
		page('*', render.content);
		page();
	});
}());
